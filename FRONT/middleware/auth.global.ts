// middleware/auth.global.ts
import { defineNuxtRouteMiddleware, navigateTo } from "#app";
import { useAuthStore } from "@/stores/auth";

export default defineNuxtRouteMiddleware(async (to, from) => {
  const authStore = useAuthStore();

  // Attendre que l'auth store soit initialisé
  await authStore.init();

  console.log("Middleware - Utilisateur actuel :", authStore.user);

  // Permettre l'accès aux pages de login et register
  if (
    !authStore.user &&
    to.path !== "/auth/login" &&
    to.path !== "/auth/register"
  ) {
    return navigateTo("/auth/login");
  }
});
