//stores/auth.ts

import { defineStore } from "pinia";
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  sendPasswordResetEmail,
  signOut,
  onAuthStateChanged,
} from "firebase/auth";
import {
  collection,
  query,
  where,
  getDocs,
  addDoc,
  Firestore,
} from "firebase/firestore";
import { useNuxtApp } from "#app";
import type { User } from "~/types/users";

export const useAuthStore = defineStore("auth", {
  state: () => ({
    user: null as User | null,
    loading: false,
    authError: null as string | null,
    emailAlreadyTakenError: null as string | null,
  }),
  actions: {
    async init() {
      const { $firestore } = useNuxtApp();
      const auth = getAuth();
      onAuthStateChanged(auth, async (firebaseUser) => {
        if (firebaseUser) {
          const q = query(
            collection($firestore as Firestore, "users"),
            where("email", "==", firebaseUser.email)
          );
          const snapshot = await getDocs(q);
          if (!snapshot.empty) {
            const userDoc = snapshot.docs[0];
            this.user = { id: userDoc.id, ...userDoc.data() } as User;
          }
        } else {
          this.user = null;
        }
        this.loading = false;
      });
    },
    async registerUser(user: Omit<User, "id" | "created_at" | "updated_at">) {
      this.loading = true;
      this.authError = null;
      this.emailAlreadyTakenError = null;
      const { $firestore } = useNuxtApp();
      const auth = getAuth();
      try {
        const q = query(
          collection($firestore as Firestore, "users"),
          where("email", "==", user.email)
        );
        const querySnapshot = await getDocs(q);

        if (!querySnapshot.empty) {
          this.emailAlreadyTakenError = "L'adresse email est déjà utilisée.";
          throw new Error(this.emailAlreadyTakenError);
        }

        const userCredential = await createUserWithEmailAndPassword(
          auth,
          user.email,
          user.password
        );
        const firebaseUser = userCredential.user;

        await addDoc(collection($firestore as Firestore, "users"), {
          id: firebaseUser.uid,
          first_name: user.first_name,
          last_name: user.last_name,
          pseudo: user.pseudo,
          email: user.email,
          created_at: new Date(),
          updated_at: new Date(),
        });
      } catch (e) {
        this.authError = (e as Error).message;
        throw e;
      } finally {
        this.loading = false;
      }
    },
    async loginUser(email: string, password: string) {
      this.loading = true;
      this.authError = null;
      const auth = getAuth();
      try {
        await signInWithEmailAndPassword(auth, email, password);
      } catch (e) {
        this.authError = (e as Error).message;
        throw e;
      } finally {
        this.loading = false;
      }
    },
    async logoutUser() {
      this.loading = true;
      this.authError = null;
      const auth = getAuth();
      try {
        await signOut(auth);
        this.user = null;
      } catch (e) {
        this.authError = (e as Error).message;
        throw e;
      } finally {
        this.loading = false;
      }
    },
    async resetPassword(email: string) {
      this.loading = true;
      this.authError = null;
      const auth = getAuth();
      try {
        await sendPasswordResetEmail(auth, email);
      } catch (e) {
        this.authError = (e as Error).message;
        throw e;
      } finally {
        this.loading = false;
      }
    },
  },
});
