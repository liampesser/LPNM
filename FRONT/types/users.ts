// types/users.ts

export interface User {
  id: string;
  first_name: string;
  last_name: string;
  pseudo?: string;
  avatar?: string;
  email: string;
  password: string;
  rank?: number; // à implémenter plus tard
  created_at: Date;
  updated_at?: Date;
}
