// types/themes.ts

export interface Theme {
  id: string;
  title: string;
  image: string;
  created_at: Date;
  updated_at?: Date;
}
