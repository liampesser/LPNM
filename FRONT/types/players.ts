// types/players.ts
import type { Team } from "./teams.js";

export interface Player {
  id: string;
  avatar?: string;
  pseudo: string;
  created_at: Date;
  updated_at?: Date;
}
