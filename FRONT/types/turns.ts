// types/turns.ts
import type { Card } from "./cards.js";

export interface Turn {
  id: string;
  team_id: string;
  player_id: string;
  words_found: Card[];
  words_skipped: Card[];
}
