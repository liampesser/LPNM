// types/cards_themes.ts
import type { Card, Theme } from "./index.js";

export interface CardHasTheme {
  id: string;
  card_id: string;
  theme_id: string;
  created_at: Date;
  updated_at?: Date;
}
