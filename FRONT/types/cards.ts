// types/cards.ts
export interface Card {
  id: string;
  word: string;
  image?: string;
  created_at: Date;
  updated_at?: Date;
}
