// types/teams.ts

import type { Player } from "./players";

export interface Team {
  id: string;
  name: string;
  image?: string;
  players: Player[];
  score: number;
  created_at: Date;
  updated_at?: Date;
}
