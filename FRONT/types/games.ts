// types/games.ts

import type { Card, Turn } from "./index";

export type RoundNumber = 1 | 2 | 3;

export interface Game {
  id: string;
  // Utilisation de string[] pour stocker uniquement les ID des équipes pour réduire la charge de données
  teams: string[];
  current_round: RoundNumber;
  current_player_id: string;
  remaining_words_ids: string[];
  rounds: {
    1: Turn[];
    2: Turn[];
    3: Turn[];
  };
  created_at: Date;
  updated_at?: Date;
}
