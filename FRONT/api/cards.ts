import { firestore } from "~/plugins/firebase";
import { collection, getDocs } from "firebase/firestore";
import type { Card } from "~/types/cards";

export const fetchCards = async (): Promise<Card[]> => {
  const cardCollection = collection(firestore, "cards");
  const snapshot = await getDocs(cardCollection);
  return snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() } as Card));
};
