import { firestore } from "~/plugins/firebase";
import { collection, getDocs } from "firebase/firestore";
import type { Turn } from "~/types/turns";

export const fetchTurns = async (gameId: string): Promise<Turn[]> => {
  const turnCollection = collection(firestore, `games/${gameId}/turns`);
  const snapshot = await getDocs(turnCollection);
  return snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() } as Turn));
};
