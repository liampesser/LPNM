import { firestore } from "~/plugins/firebase";
import { collection, getDocs } from "firebase/firestore";
import type { Player } from "~/types/players";

export const fetchPlayers = async (): Promise<Player[]> => {
  const playerCollection = collection(firestore, "players");
  const snapshot = await getDocs(playerCollection);
  return snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() } as Player));
};
