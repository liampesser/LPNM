import { firestore } from "~/plugins/firebase";
import { collection, getDocs } from "firebase/firestore";
import type { Theme } from "~/types/themes";

export const fetchThemes = async (): Promise<Theme[]> => {
  const themeCollection = collection(firestore, "themes");
  const snapshot = await getDocs(themeCollection);
  return snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() } as Theme));
};
