import { firestore } from "~/plugins/firebase";
import { collection, getDocs } from "firebase/firestore";
import type { User } from "~/types/users";

export const fetchUsers = async (): Promise<User[]> => {
  const userCollection = collection(firestore, "users");
  const snapshot = await getDocs(userCollection);
  return snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() } as User));
};
