import { firestore } from "~/plugins/firebase";
import { collection, getDocs } from "firebase/firestore";
import type { Team } from "~/types/teams";

export const fetchTeams = async (): Promise<Team[]> => {
  const teamCollection = collection(firestore, "teams");
  const snapshot = await getDocs(teamCollection);
  return snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() } as Team));
};
