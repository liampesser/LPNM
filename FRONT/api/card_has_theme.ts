import { firestore } from "~/plugins/firebase";
import { collection, getDocs, addDoc, query, where } from "firebase/firestore";
import type { CardHasTheme } from "~/types/card_has_theme";

// Fetch all CardHasTheme relationships
export const fetchCardHasThemes = async (): Promise<CardHasTheme[]> => {
  const cardHasThemeCollection = collection(firestore, "card_has_theme");
  const snapshot = await getDocs(cardHasThemeCollection);
  return snapshot.docs.map(
    (doc) => ({ id: doc.id, ...doc.data() } as CardHasTheme)
  );
};

// Fetch CardHasTheme relationships by theme_id
export const fetchCardsByTheme = async (
  theme_id: string
): Promise<CardHasTheme[]> => {
  const cardHasThemeCollection = collection(firestore, "card_has_theme");
  const q = query(cardHasThemeCollection, where("theme_id", "==", theme_id));
  const snapshot = await getDocs(q);
  return snapshot.docs.map(
    (doc) => ({ id: doc.id, ...doc.data() } as CardHasTheme)
  );
};

// Fetch CardHasTheme relationships by card_id
export const fetchThemesByCard = async (
  card_id: string
): Promise<CardHasTheme[]> => {
  const cardHasThemeCollection = collection(firestore, "card_has_theme");
  const q = query(cardHasThemeCollection, where("card_id", "==", card_id));
  const snapshot = await getDocs(q);
  return snapshot.docs.map(
    (doc) => ({ id: doc.id, ...doc.data() } as CardHasTheme)
  );
};

// Create a new CardHasTheme relationship
export const createCardHasTheme = async (
  cardHasThemeData: CardHasTheme
): Promise<void> => {
  const cardHasThemeCollection = collection(firestore, "card_has_theme");
  await addDoc(cardHasThemeCollection, cardHasThemeData);
};
