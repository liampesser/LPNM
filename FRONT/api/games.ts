import { firestore } from "~/plugins/firebase";
import { collection, addDoc, getDocs } from "firebase/firestore";
import type { Game } from "~/types/games";

export const createGame = async (gameData: Game): Promise<void> => {
  const gameCollection = collection(firestore, "games");
  await addDoc(gameCollection, gameData);
};

export const fetchGames = async (): Promise<Game[]> => {
  const gameCollection = collection(firestore, "games");
  const snapshot = await getDocs(gameCollection);
  return snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() } as Game));
};
