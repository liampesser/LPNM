/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./components/**/*.{vue,js}", // Chemins vers tes fichiers Vue et JS dans les composants
    "./layouts/**/*.vue", // Chemins vers tes fichiers Vue dans les layouts
    "./pages/**/*.vue", // Chemins vers tes fichiers Vue dans les pages
    "./plugins/**/*.{js,ts}", // Chemins vers tes fichiers JS ou TS dans les plugins
    "./nuxt.config.{js,ts}", // Le fichier de configuration de Nuxt
  ],
  theme: {
    extend: {
      colors: {
        primary: "#2EBBAA",
        secondary: "#11776E",
        accent: '#248ed3',
        light: "#ffffff",
        neutral: "#3c454d",
        dark: "#232836",
        "neutral-light": "#5e7399",
        "neutral-dark": "#31384B",
        "non-selected": "#b0b0b0",
             
        like: "#f94449",
        error: "#e61c22",
        warning: "#f59c0b",
        success: "#00a061",
        info: "#00adcf",
      },
      backgroundImage: {
        //'pages': "url('./assets/images/bg_pages.webp')",
        //'header-banner': "url('./assets/images/header_banner.png')",
      }
    },
  },
  plugins: [require('daisyui')],
  daisyui: {
    themes: [
      {
        mytheme: {
          "primary": "#1bb1a2",
          "secondary": "#64e4d1",
          "accent": "#248ed3",
          "light": "#ffffff",
          "neutral": "#31384B",
          "dark": "#232836",
          "neutral-light": "#5e7399",
          "neutral-dark": "#232836",
          "non-selected": "##94a3b8",
          
          "like": "#f94449",
          "error": "#e61c22",
          "warning": "#f59c0b",
          "success": "#00a061",
          "info": "#00adcf",
        },
      },
    ],
  },
}
