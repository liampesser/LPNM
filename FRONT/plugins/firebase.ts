import { defineNuxtPlugin, useRuntimeConfig } from "#app";
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore, Firestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
import type { FirebaseOptions } from "firebase/app";

let firebaseApp: ReturnType<typeof initializeApp>;
let auth: ReturnType<typeof getAuth>;
let firestore: Firestore;
let storage: ReturnType<typeof getStorage>;

export default defineNuxtPlugin((nuxtApp) => {
  const config = useRuntimeConfig().public.firebaseConfig as FirebaseOptions;

  if (!config) {
    throw new Error("Firebase configuration is missing");
  }

  firebaseApp = initializeApp(config);
  auth = getAuth(firebaseApp);
  firestore = getFirestore(firebaseApp) as Firestore;
  storage = getStorage(firebaseApp);

  nuxtApp.provide("firebase", firebaseApp);
  nuxtApp.provide("auth", auth);
  nuxtApp.provide("firestore", firestore);
  nuxtApp.provide("storage", storage);
});

export { firebaseApp, auth, firestore, storage };
