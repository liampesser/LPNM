// composables/useValidation.ts

import {
  helpers,
  required,
  email,
  minLength,
  sameAs,
} from "@vuelidate/validators";
import type { Ref } from "vue";

export const errorValidationMessages = {
  required: "Ce champ est requis.",
  passwordMinLength: "Le mot de passe doit contenir au moins 8 caractères.",
  email: "L'adresse mail est invalide.",
  sameAsPassword: "Les mots de passe ne correspondent pas.",
  emailAlreadyTaken: "L'adresse mail est déjà utilisée.",
};

export const requiredValidator = helpers.withMessage(
  errorValidationMessages.required,
  required
);
export const emailValidator = helpers.withMessage(
  errorValidationMessages.email,
  email
);
export const passwordMinLengthValidator = helpers.withMessage(
  errorValidationMessages.passwordMinLength,
  minLength(8)
);
export const sameAsPasswordValidator = (password: Ref<string>) =>
  helpers.withMessage(errorValidationMessages.sameAsPassword, sameAs(password));
